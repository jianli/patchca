<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<table>
		<tr>
			<!-- patchca.png和servlet中的<url-pattern>对应 -->
			<td><img src="patchca.png" alt="验证码"
				style="cursor: pointer; vertical-align: text-bottom;"
				onclick="this.src=this.src+'?'+Math.random();"></td>
			<td valign="top">
				<form method="POST">
					<br> 验证码:<input type="text" name="patchcafield"><br />
					<input type="submit" name="submit">
				</form>
			</td>
		</tr>
	</table>

	<%
		//"PATCHCA"和servlet中定义的session保存位置对应
		String c = (String) session.getAttribute("captchaToken");
		String parm = (String) request.getParameter("patchcafield");
		out.println("Parameter: " + parm + " ? Session Key: " + c + " : ");
		if (c != null && parm != null) {
			if (c.equalsIgnoreCase(parm)) {
				out.println("<b>true</b>");
			} else {
				out.println("<b>false</b>");
			}
		}
	%>

</body>
</html>